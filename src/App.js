import { useState, useEffect } from "react";
import "./App.css";

function App() {
  const [selected, setSelected] = useState(null);
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [search, setSearch] = useState("");
  const [limit, setLimit] = useState(10);
  const [noData, setNodata] = useState(false);
  const [loadingList, setLoadingList] = useState(false);

  const callApi = () => {
    fetch("https://api.thecatapi.com/v1/breeds?limit=" + limit)
      .then((res) => res.json())
      .then((result) => {
        setData(result);
        if (result.length === 0) setNodata(true);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        setLoading(false);
        setLoadingList(false);
      });
  };

  const callApi2 = () => {
    fetch("https://api.thecatapi.com/v1/breeds")
      .then((res) => res.json())
      .then((result) => {
        setData(result);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        setLoading(false);
        setLoadingList(false);
      });
  };

  const loader = () => {
    return (
      <div className="loader">
        <h2>Loading ...</h2>
      </div>
    );
  };

  window.onscroll = () => {
    setLoadingList(true);
    if (
      window.innerHeight + document.documentElement.scrollTop ===
      document.body.scrollHeight
    ) {
      if (loadingList) {
        setLimit(limit + 10);
      } else {
        setLoadingList(false);
      }
    }
  };

  const toggle = (i) => {
    if (selected === i) {
      return setSelected(null);
    }
    setSelected(i);
  };
  function handleSearch(e) {
    callApi2();
    //setLoadingList(false)
    setSearch(e.target.value);
  }

  useEffect(() => {
    callApi();
  }, [limit]);
  return (
    <div className="App">
      {loading ? (
        loader()
      ) : (
        <div className="accordion">
          <div className="headerSearch">
            <input
              className="searchbar"
              type="text"
              placeholder="search by name"
              onChange={handleSearch}
            ></input>
          </div>
          {data
            .filter((item) => {
              if (search === "") {
                return item;
              } else if (
                item.name.toLowerCase().includes(search.toLowerCase())
              ) {
                return item;
              }
              return false
            })
            .map((item, i) => (
              <div className="item" key={item?.id?.toString()}>
                <div
                  className="title"
                  style={{ cursor: "pointer" }}
                  onClick={() => toggle(i)}
                >
                  <h2>{item.name}</h2>
                  <span>{selected === i ? "-" : "+"}</span>
                </div>
                <div className={selected === i ? "content show" : "content"}>
                  <img className="image" src={item?.image?.url} alt="description_img"></img>
                  <div>
                  <h3>Description:</h3>
                  <h3>{item.description}</h3>
                  <h3>Temperament: {item.temperament}</h3>
                  <h3>Origin: {item.origin}</h3>
                  <h3>Life span: {item.life_span}</h3>
                  <div className="boxContent">
                  <h3 className="star">Adaptability: {item.adaptability} &#9733;</h3>
                  <h3 className="star">Affection level: {item.affection_level} &#9733;</h3>
                  <h3 className="star">Child friendly: {item.child_friendly} &#9733;</h3>
                  <h3 className="star">Dog friendly: {item.dog_friendly} &#9733;</h3>
                  <h3 className="star">Energy level: {item.energy_level} &#9733;</h3>
                  <h3 className="star">Grooming: {item.grooming} &#9733;</h3>
                  <h3 className="star">Health issues: {item.health_issues} &#9733;</h3>
                  <h3 className="star">intelligence: {item.intelligence} &#9733;</h3>
                  <h3 className="star">Shedding level: {item.shedding_level} &#9733;</h3>
                  <h3 className="star">Strange rfriendly: {item.stranger_friendly} &#9733;</h3>
                  <h3 className="star">Vocalisation: {item.vocalisation} &#9733;</h3>
                  <h3 className="star">Experimental: {item.experimental} &#9733;</h3>
                  <h3 className="star">Hairless: {item.hairless} &#9733;</h3>
                  <h3 className="star">Natural: {item.natural} &#9733;</h3>
                  <h3 className="star">Rare: {item.rare} &#9733;</h3>
                  <h3 className="star">suppressed_tail: {item.suppressed_tail} &#9733;</h3>
                  <h3 className="star">Short legs: {item.short_legs} &#9733;</h3>
                  <h3 className="star">Hypoallergenic: {item.hypoallergenic} &#9733;</h3>
                  </div>
                  
                  </div>
                </div>
              </div>
            ))}
          {loadingList ? <div className="text-center">loading ...</div> : ""}
        </div>
      )}
    </div>
  );
}

export default App;
